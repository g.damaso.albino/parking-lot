from django.db import models

import math


# Create your models here.
class Person(models.Model):
	"""docstring for Person"""
	name = models.CharField(max_length=100)
	address = models.CharField(max_length=200)
	phone = models.IntegerField()

	def __str__(self):
		return self.name	


class Brand(models.Model):
	"""docstring for Brand"""
	name = models.CharField(max_length=50)

	def __str__(self):
		return self.name
		

class Vehicle(models.Model):
	"""docstring for Ve"""
	brand = models.ForeignKey(Brand, on_delete=models.CASCADE)
	owner = models.ForeignKey(Person, on_delete=models.CASCADE)
	plate = models.CharField(max_length=7)
	color = models.CharField(max_length=15)
	observations = models.TextField()

	def __str__(self):
		return self.brand.name + ' - ' + self.plate


class Parameters(models.Model):
	price_hour = models.DecimalField(max_digits=5, decimal_places=2)	
	price_month = models.DecimalField(max_digits=6, decimal_places=2)	

	def __str__(self):
		return "General Parameters"


class MovDaily(models.Model):
	check_in = models.DateTimeField(auto_now=False)
	check_out = models.DateTimeField(auto_now=False, null=True, blank=True)
	price_hour = models.DecimalField(max_digits=5, decimal_places=2)	
	vehicle = models.ForeignKey(Vehicle, on_delete=models.CASCADE)
	payment_status = models.BooleanField(default=False)

	def total_hours(self):
		return math.ceil((self.check_out - self.check_in).total_seconds() / 3600) 

	def total(self):
		return self.price_hour * self.total_hours()

	def __str__(self):
		return self.vehicle.plate


class Monthly(models.Model):
	vehicle = models.ForeignKey(Vehicle, on_delete=models.CASCADE)
	begin_day = models.DateField()
	price_month = models.DecimalField(max_digits=6, decimal_places=2)

	def __str__(self):
		return str(self.vehicle) + '-' + str(self.begin_day)


class MovMonthly(models.Model):
	monthly = models.ForeignKey(Monthly, on_delete=models.CASCADE)
	dt_payment = models.DateField(null=True)
	total = models.DecimalField(max_digits=6, decimal_places=2)

	def __str__(self):
		return str(self.monthly) + '-' + str(self.total)
