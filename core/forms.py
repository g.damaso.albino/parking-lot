from django.forms import ModelForm
from .models import (
	Person,
	Vehicle,
	MovDaily,
	Monthly,
	MovMonthly
)


class PersonForm(ModelForm):
	class Meta:
		model = Person
		fields = '__all__'


class VehicleForm(ModelForm):
	class Meta:
		model = Vehicle
		fields = '__all__'


class MovDailyForm(ModelForm):
	class Meta:
		model = MovDaily
		fields = '__all__'


class MonthlyForm(ModelForm):
	class Meta:
		model = Monthly
		fields = '__all__'


class MovMonthlyForm(ModelForm):
	class Meta:
		model = MovMonthly
		fields = '__all__'
