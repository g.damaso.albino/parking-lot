"""parkinglot URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path
from .views import (
    home,
    list_people,
    list_vehicles,
    list_movdaily,
    list_movmonthly,
    list_monthly,
    new_person,
    new_vehicle,
    new_movdaily,
    new_monthly,
    new_movmonthly,
    update_person,
    update_vehicle,
    update_movdaily,
    update_monthly,
    update_movmonthly,
    delete_person,
    delete_vehicle,
    delete_movdaily,
    delete_monthly,
    delete_movmonthly,
)

urlpatterns = [
    path('', home, name='home'),
    path('people', list_people, name='core_list_people'),
    path('new_person', new_person, name='core_new_person'),
    path('update_person/<int:id>', update_person, name='core_update_person'),
    path('delete_person/<int:id>', delete_person, name='core_delete_person'),

    path('vehicles', list_vehicles, name='core_list_vehicles'),
    path('new_vehicle', new_vehicle, name='core_new_vehicle'),
    path('update_vehicle/<int:id>', update_vehicle, name='core_update_vehicle'),
    path('delete_vehicle/<int:id>', delete_vehicle, name='core_delete_vehicle'),

    path('movdaily', list_movdaily, name='core_list_movdaily'),
    path('new_movdaily', new_movdaily, name='core_new_movdaily'),
    path('update_movdaily/<int:id>', update_movdaily, name='core_update_movdaily'),
    path('delete_movdaily/<int:id>', delete_movdaily, name='core_delete_movdaily'),

    path('monthly', list_monthly, name='core_list_monthly'),
    path('new_monthly', new_monthly, name='core_new_monthly'),
    path('update_monthly/<int:id>', update_monthly, name='core_update_monthly'),
    path('delete_monthly/<int:id>', delete_monthly, name='core_delete_monthly'),

    path('movmonthly', list_movmonthly, name='core_list_movmonthly'),
    path('new_movmonthly', new_movmonthly, name='core_new_movmonthly'),
    path('update_movmonthly/<int:id>', update_movmonthly, name='core_update_movmonthly'),
    path('delete_movmonthly/<int:id>', delete_movmonthly, name='core_delete_movmonthly'),
]
