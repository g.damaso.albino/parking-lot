from django.contrib import admin
from .models import (
	Brand, 
	Vehicle, 
	Person, 
	Parameters, 
	MovDaily,
	Monthly,
	MovMonthly
)
# Register your models here.


class MovDailyAdmin(admin.ModelAdmin):
	list_display = ('check_in', 'check_out', 'vehicle',	 'price_hour', 'payment_status', 'total_hours', 'total', 'test')

	def test(self, obj):
		return "test"


class MovMonthlyAdmin(admin.ModelAdmin):
	list_display = ('monthly', 'dt_payment', 'total')


admin.site.register(Brand)
admin.site.register(Vehicle)
admin.site.register(Person)
admin.site.register(Parameters)
admin.site.register(Monthly)
admin.site.register(MovMonthly, MovMonthlyAdmin)
admin.site.register(MovDaily, MovDailyAdmin)
