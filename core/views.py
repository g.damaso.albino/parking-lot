from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required

from .models import (
	Person,
	Vehicle,
	MovDaily,
	MovMonthly,
	Monthly
)

from .forms import (
	PersonForm,
	VehicleForm,
	MovDailyForm,
	MonthlyForm,
	MovMonthlyForm
)


@login_required()
def home(request):
	context = {'message': 'Hello World'}
	return render(request, 'core/index.html', context)


@login_required()
def list_people(request):
	people = Person.objects.all()
	form = PersonForm()
	data = {'people': people, 'form': form}
	return render(request, 'core/list_people.html', data)


@login_required()
def new_person(request):
	form = PersonForm(request.POST or None)
	if form.is_valid():
		form.save()
	return redirect('core_list_people')


@login_required()
def update_person(request, id):
	data = {}
	person = Person.objects.get(id=id)
	form = PersonForm(request.POST or None, instance=person)
	data['person'] = person
	data['form'] = form

	if request.method == 'POST':
		if form.is_valid():
			form.save()
			return redirect('core_list_people')
	else:
		return render(request, 'core/update_person.html', data)


@login_required()
def delete_person(request, id):
	person = Person.objects.get(id=id)

	if request.method == 'POST':
		person.delete()
		return redirect('core_list_people')
	else:
		return render(request, 'core/delete_confirm_obj.html', {'obj': person})


@login_required()
def list_vehicles(request):
	vehicles = Vehicle.objects.all()
	form = VehicleForm()
	data = {'vehicles': vehicles, 'form': form}
	return render(request, 'core/list_vehicles.html', data)


@login_required()
def new_vehicle(request):
	form = VehicleForm(request.POST or None)
	if form.is_valid():
		form.save()
	return redirect('core_list_vehicles')


@login_required()
def update_vehicle(request, id):
	data = {}
	vehicle = Vehicle.objects.get(id=id)
	form = VehicleForm(request.POST or None, instance=vehicle)
	data['vehicle'] = vehicle
	data['form'] = form

	if request.method == 'POST':
		if form.is_valid():
			form.save()
			return redirect('core_list_vehicles')
	else:
		return render(request, 'core/update_vehicle.html', data)


@login_required()
def delete_vehicle(request, id):
	vehicle = Vehicle.objects.get(id=id)

	if request.method == 'POST':
		vehicle.delete()
		return redirect('core_list_vehicles')
	else:
		return render(request, 'core/delete_confirm_obj.html', {'obj': vehicle})


@login_required()
def list_movdaily(request):
	daily_mov = MovDaily.objects.all()
	form = MovDailyForm()
	data = {'daily_mov': daily_mov, 'form': form}
	return render(request, 'core/list_movdaily.html', data)


@login_required()
def new_movdaily(request):
	form = MovDailyForm(request.POST or None)
	if form.is_valid():
		form.save()
	return redirect('core_list_movdaily')


@login_required()
def update_movdaily(request, id):
	data = {}
	movdaily = MovDaily.objects.get(id=id)
	form = MovDailyForm(request.POST or None, instance=movdaily)
	data['movdaily'] = movdaily
	data['form'] = form

	if request.method == 'POST':
		if form.is_valid():
			form.save()
			return redirect('core_list_movdaily')
	else:
		return render(request, 'core/update_movdaily.html', data)


@login_required()
def delete_movdaily(request, id):
	movdaily = MovDaily.objects.get(id=id)

	if request.method == 'POST':
		movdaily.delete()
		return redirect('core_list_movdaily')
	else:
		return render(request, 'core/delete_confirm_obj.html', {'obj': movdaily})


@login_required()
def list_monthly(request):
	monthly = Monthly.objects.all()
	form = MonthlyForm()
	data = {'monthly': monthly, 'form': form}
	return render(request, 'core/list_monthly.html', data)


@login_required()
def new_monthly(request):
	form = MonthlyForm(request.POST or None)
	if form.is_valid():
		form.save()
	return redirect('core_list_monthly')


@login_required()
def update_monthly(request, id):
	data = {}
	monthly = Monthly.objects.get(id=id)
	form = MonthlyForm(request.POST or None, instance=monthly)
	data['monthly'] = monthly
	data['form'] = form

	if request.method == 'POST':
		if form.is_valid():
			form.save()
		return redirect('core_list_monthly')
	else:
		return render(request, 'core/update_monthly.html', data)


@login_required()
def delete_monthly(request, id):
	monthly = Monthly.objects.get(id=id)

	if request.method == 'POST':
		monthly.delete()
		return redirect('core_list_monthly')
	else:
		return render(request, 'core/delete_confirm_obj.html', {'obj': monthly})


@login_required()
def list_movmonthly(request):
	monthly_mov = MovMonthly.objects.all()
	form = MovMonthlyForm()
	data = {'monthly_mov': monthly_mov, 'form': form}
	return render(request, 'core/list_movmonthly.html', data)


@login_required()
def new_movmonthly(request):
	form = MonthlyForm(request.POST or None)
	if form.is_valid():
		form.save()
	return redirect('core_list_movmonthly')


@login_required()
def update_movmonthly(request, id):
	data = {}
	movmonthly = MovMonthly.objects.get(id=id)
	form = MonthlyForm(request.POST or None, instance=movmonthly)
	data['movmonthly'] = movmonthly
	data['form'] = form

	if request.method == 'POST':
		if form.is_valid():
			form.save()
		return redirect('core_list_movmonthly')
	else:
		return render(request, 'core/update_movmonthly.html', data)


@login_required()
def delete_movmonthly(request, id):
	movmonthly = MovMonthly.objects.get(id=id)

	if request.method == 'POST':
		movmonthly.delete()
		return redirect('core_list_movmonthly')
	else:
		return render(request, 'core/delete_confirm_obj.html', {'obj': movmonthly})
